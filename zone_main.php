<?php


print '<div class="navbar">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dns.php">DNS Manager v0.1.1</a>
          <div class="navbar-content">
            <ul class="nav">
              <li class="active">
                <a href="#">Domains</a> 
              </li>
              <li>
                <a href="#">Profile</a> 
              </li>
              <li>
                <a href="#">Administration</a> 
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <ul class="nav  nav-pills nav-stacked">
			<li>
              <a href="dns.php">Domain List</a> 
            </li>
            <li class="active">
              <a href="#">DNS Records</a> 
            </li>
            
          </ul>
          <div class="well">
            <p>
              <b>Tip:</b> You can view and edit DNS entries from this page.</p>
			<p>
				Dots at the end of records are not mandatory while editing. Script will do a basic error checking for missing dots and quotes (TXT records).
			</p>
			<p>
				Returning to the Domain list will abandon all unsaved changes for ever, and revert to the original zone files in the filesystem. Make sure to save your data.
			</p>
          </div>
        </div>
        
		
';
?>          

          
