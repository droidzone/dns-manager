<?php
$AUTHSCRIPT="/var/password_protect.php";
if (file_exists($AUTHSCRIPT)) 
{
	include($AUTHSCRIPT);	
}
#Defined for default BIND9 location on Debian
$CONF_FILE="/etc/bind/named.conf.local";
if (!file_exists($CONF_FILE)) 
{
	#Maybe testing on WAMP
	$CONF_FILE="named.conf.local";
}

function strallpos($haystack,$needle,$offset = 0){ 
    $result = array(); 
    for($i = $offset; $i<strlen($haystack); $i++){ 
        $pos = strpos($haystack,$needle,$i); 
        if($pos !== FALSE){ 
            $offset =  $pos; 
            if($offset >= $i){ 
                $i = $offset; 
                $result[] = $offset; 
            } 
        } 
    } 
    return $result; 
} 

function hp ($lin)
{
	#print $lin;
	#"<p>".$lin."</p>";
}

function hphead ()
{
	include 'static/header.php';
	include 'static/main.php';
	#print "<html><head></head><body>";
}	
	
function hpfooter ()
{
	print "</body></html>";
}

function zonesearch ($haystack)
{
	
	$needle='"';
	print "Searching the haystack:".$haystack." for ".$needle;
	$fq=strpos($haystack,$needle);
	$gq=strpos($haystack,$needle,$fq);
	print "Found at positions ".$fq." and ".$gq;
}
	
function ReadDomainConfFile ()
{
	global $CONF_FILE;
	$filename=$CONF_FILE;
	if (is_readable($filename))
	{
		#print "Read it";
		$filecont=file_get_contents($filename);		
		$arr=explode ( "zone ", $filecont);
		hp("");
		for ($i=0;$i<count($arr);$i++)
		{
			if ( ( preg_match("/^\//", $arr[$i])) || ( preg_match("/^#/", $arr[$i])) )
			{
				
				unset ($arr[$i]);
			}	
		}
		
		$arr=array_values($arr);
		for ($i=0;$i<count($arr);$i++)
		{
			$a=($arr[$i]);
		}
		print '
		<tbody>
		 ';
		for ($i=0;$i<count($arr);$i++)
		{
			$haystack=$arr[$i];
			$needle='"';		
			$a=stripos($haystack,$needle);		
			$b=stripos($haystack,$needle,$a+1);			
			$domain=substr($haystack,$a+1,$b-1);
			$domtext=substr($haystack,$b+1);			
			$index=$b+1;
			$needle="type";
			$typest=stripos($domtext,"type");
			$typend=stripos($domtext,";",$typest+4);
			$filtext=substr($domtext,$typend+1);			
			$filest=stripos($filtext,"file");
			$filend=stripos($filtext,";",$typest+4);
              print '<tr>';			 
			$filename=substr($filtext,$filest+1+4+1,$filend-($filest+1+4+2));
			print '<td>'.($i+1).'</td><td>'.
			'<a>'.$domain.'</a>'.
			'</td><td>'.
			substr($domtext,$typest+1+4,$typend-($typest+1+4)).'</td><td>'.
			substr($filtext,$filest+1+4+1,$filend-($filest+1+4+2)).'</td>
			<input type="hidden" name="ZONEFILE['.$i.']" value="'.$filename.'">
			<td>
			
			<button type="submit" name="EDIT_ZONE['.$i.']" class="btn btn-primary"/>Edit zone</button>
			</td>		
			
			</tr>';
			$total=$i+1;			
		}	
		print '
		
		</tbody>
		
          </table>
		<div>Showing a total of '.$total.' domains </div>
        </div>
		
      </div>
    </div>';
	

		
	}
	else
	{
		hp("File is NOT readable");
	}	
}	
	
hphead();
ReadDomainConfFile();
hpfooter();



?>