<?php
	global $DEBUGON;
	if ($DEBUGON)
			{
				debug_print_backtrace();
				var_dump($_POST);			
			}
	global $DEBUGMSG;
	global $MAIN_DOMAINNAME,$MAIN_NS,$MAIN_ADMINMAIL,$NS;
	global $A_REC, $A_VAL;
	global $MX_PREF,$MX_VAL;
	global $CN_PREF,$CN_VAL,$CN_NAME;
	global $TXT_VAL, $TXT_PREF;
	global $CONF_FILE,$DOMAIN_SERIAL,$DOMAIN_REFRESH, $DOMAIN_RETRY, $DOMAIN_EXPIRE, $DOMAIN_NEGTTL;
	global $NO_TXT_BANNER;
	#Delete Null NS
	
	while (list($key, $val) = each($NS))  
	{			
		if ( !isset ($NS[$key]) || $NS[$key]=="")
		{
			unset($NS[$key]);
		}
		else
		{					
			if ( !( substr( $NS[$key] , -1) == "." ) )
			{
				$NS[$key] .= ".";				
			}			
		}
	}
	$NS=array_values($NS);	

	#Delete Null MX
	while (list($key, $val) = each($MX_VAL))  
	{			
		if ( !isset ($MX_VAL[$key]) || $MX_VAL[$key]=="")
		{
			unset($MX_VAL[$key]);
			unset($MX_PREF[$key]);
		}
		else
		{					
			if ( !( substr( $MX_VAL[$key] , -1) == "." ) )
			{
				$MX_VAL[$key] .= ".";				
			}			
		}
	}
	$MX_VAL=array_values($MX_VAL);
	$MX_PREF=array_values($MX_PREF);	
	
	#Delete Null A	
	while (list($key, $val) = each($A_VAL))  
	{			
		if ( !isset ($A_VAL[$key]) || $A_VAL[$key]=="")
		{			
			unset($A_REC[$key]);
			unset($A_VAL[$key]);
		}
		else
		{			
			if ( !($A_REC[$key] =="" ) )
			{				
				if ( !( substr( $A_REC[$key] , -1) == "." ) )
				{
					$A_REC[ $key ] .= ".";						
				}		
			}
		}
	}	
	$A_REC=array_values($A_REC);
	$A_VAL=array_values($A_VAL);	
	
	#Delete Null CNAME
	while (list($key, $val) = each($CN_NAME))  
	{			
		if ( !isset ($CN_NAME[$key]) || $CN_NAME[$key]=="")
		{
			unset($CN_NAME[$key]);
			unset($CN_VAL[$key]);
			unset($CN_PREF[$key]);
			
		}
		else
		{					
			if ( !( substr( $CN_NAME[$key] , -1) == "." ) )
			{
				$CN_NAME[$key] .= ".";				
			}			
		}
	}
	$CN_NAME=array_values($CN_NAME);
	$CN_VAL=array_values($CN_VAL);	
	$CN_PREF=array_values($CN_PREF);
	
	#Delete Null TXT
	while (list($key, $val) = each($TXT_VAL))  
	{			
		if ( !isset ($TXT_VAL[$key]) || $TXT_VAL[$key]=="")
		{
			unset($TXT_VAL[$key]);
			unset($TXT_PREF[$key]);
		}
		else
		{
			$TXT_VAL[$key]=trim($TXT_VAL[$key], " ");
			if ( !(substr( $TXT_VAL[$key] , -1) == '"') )
			{				
				$TXT_VAL[$key] .= '"';
			}
			if ( !(substr( $TXT_VAL[$key] , 0, 1) == '"') )
			{				
				$TXT_VAL[$key] = '"' . $TXT_VAL[$key];

			}
		}
	}
	$TXT_VAL=array_values($TXT_VAL);
	$TXT_PREF=array_values($TXT_PREF);	
?>