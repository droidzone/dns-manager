	<?php
	global $DEBUGON;
	if ($DEBUGON) 		{
		debug_print_backtrace();
		var_dump($_POST);			
	}
	global $DEBUGMSG;
	global $DATASAVED, $DATAUNSAVED_ALERT;
	global $BANNER,$DBANNER,$NO_NS_BANNER,$NO_TXT_BANNER;
	global $NO_CN_BANNER,$NO_A_BANNER,$NO_MX_BANNER;
	global $SOA_LINE,$MAIN_DOMAINNAME,$MAIN_NS,$MAIN_ADMINMAIL,$SEC_BRACKET,$NS,$MX,$CN,$TXT,$A;
	global $A_REC, $A_VAL;
	global $MX_PREF,$MX_VAL,$CN_PREF,$CN_VAL,$CN_NAME;
	global $TXT_VAL, $TXT_PREF;
	global $CONF_FILE,$DOMAIN_SERIAL,$DOMAIN_REFRESH, $DOMAIN_RETRY, $DOMAIN_EXPIRE, $DOMAIN_NEGTTL;
	#print_r($BANNER);
	
	global $MENU_NAME,$MENU_URL;		
	$MENU_NAME=array("Domains","DNS records");	
	BreadCrumb();	
	
	print $BANNER;
	print $DEBUGMSG;
	$BANNER='';	
	print "<h4>Domain Name:".$MAIN_DOMAINNAME."<br /></h4><h5>Zone file:$CONF_FILE</h5>";			
	AlertIfUnsaved();
	print '
	<script src="addField.js"></script>

	<form action="" method="post" name="FormProcessor">
	<input type="hidden" name="MAIN_DOMAINNAME" value="'.$MAIN_DOMAINNAME.'">
	<input type="hidden" name="CONF_FILE" value="'.$CONF_FILE.'">
	<div id="infomsg">
		<div class="alert alert-info hidden-phone">
					<a data-dismiss="alert" class="close">×</a>		
					<div id="info_actual"></div>								
	  	</div>
	</div>	
	
	<table class="table">
		<thead>
		  <tr>
			<th>Serial</th>
			<th>Refresh</th> 
			<th>Retry</th>
			<th>Expiry</th>
			<th>Negative TTL</th>
		  </tr>
		</thead>';
	
	#print '<tr><td>'.$DOMAIN_SERIAL.'</td>';
	print '<tr><td><input type="text"  name="DOMAIN_SERIAL" style="width: 100px;" value="'.$DOMAIN_SERIAL.'"></td>';
	print '<td><input type="text"  name="DOMAIN_REFRESH" style="width: 50px;" value="'.$DOMAIN_REFRESH.'"></td>';
	print '<td><input type="text"  name="DOMAIN_RETRY" style="width: 50px;" value="'.$DOMAIN_RETRY.'"></td>';
	print '<td><input type="text"  name="DOMAIN_EXPIRE" style="width: 50px;" value="'.$DOMAIN_EXPIRE.'"></td>';
	print '<td><input type="text"  name="DOMAIN_NEGTTL" style="width: 50px;" value="'.$DOMAIN_NEGTTL.'"></td></tr>';
	print '<tr><td>Main Nameserver</td><td><input type="text"  name="MAIN_NS" style="width: 200px;" value="'.$MAIN_NS.'"></td></tr>';
	print '<tr><td>Administrator Email</td><td><input type="text"  name="MAIN_ADMINMAIL" style="width: 200px;" value="'.$MAIN_ADMINMAIL.'"></td></tr>';
	print '</table>';
	
	print '<h4>Records</h4>';
	if (!count($NS)>0)
	{
		$BANNER = $NO_NS_BANNER;
		print $BANNER;
		$BANNER = '';
		$NS[0]="";	 
	}
	print '
		
		<table class="table" id="nameservers">		
		';

	#Nameserver records		
	print '
	
		<thead >
		  <tr >
			<th>Type</th>
			<th>Value</th>   
			
		  </tr>
		</thead>';
	

	for ($i=0;$i<count($NS);$i++)
	{	
		print '
		<tr id="NSTR'.$i.'"> 
		<td>NS</td>
		<td><input type="text" name="NS['.$i.']" value="'.$NS[$i].'">	';
			
		#<button type="submit" name="delete_NS['.$i.']" value="Delete record" class="btn btn-danger"/>Delete</button>
		
		print '
		<a class="btn btn-danger" onclick="DelField(this.id);" id="NS_D'.$i.'" >
			<span class="btn-label">Delete		
		</span>
		</a>			
					
		<a class="btn btn-primary" onclick="addField(\'nameservers\',\'NS\',10);" id="NS_A'.$i.'" >
			<span class="btn-label">Add		
		</span>
		</a>
		</td>		
		</tr>';			
	}

	print '</table>';
	#MX records	
	if (!count($MX_VAL)>0)
	{
		$BANNER = $NO_MX_BANNER;
		print $BANNER;
		$BANNER = '';
		$MX_PREF[0]="";
		$MX_VAL[0]="";	
	}	

		print '
		<table class="table" id="mxservers">
		<thead>
			  <tr>
				<th>Type</th>
				<th>Preference</th>
				<th>Value</th>                
			  </tr>
			</thead>';
		for ($i=0;$i<count($MX_VAL);$i++)
		{	
					
			print '
			<tr id="MXTR'.$i.'"> 
			<td>MX</td>
			<td><input type="text" name="MX_PREF['.$i.']" style="width: 30px;" value="'.$MX_PREF[$i].'"></td><td>'.
			'<input type="text" name="MX_VAL['.$i.']" value="'.$MX_VAL[$i].'">';
			#<button type="submit" name="delete_MX['.$i.']" value="Delete record" class="btn btn-danger"/>Delete</button>	';			
			#Create a Delete Button		
			print '
			<a class="btn btn-danger" onclick="DelField(this.id);" id="MX_D'.$i.'" >
				<span class="btn-label">Delete	
			</span>
			</a>';
			#Create an Add Button			
			print '			
			<a class="btn btn-primary" onclick="addField(\'mxservers\',\'MX\',10);" id="MX_A'.$i.'" >
				<span class="btn-label">Add		
			</span>
			
			</td></tr>';
					
		}

	print '</table>';
	
	#A Records
	if (!count($A_VAL)>0)
	{
		$BANNER = $NO_A_BANNER;
		print $BANNER;
		$BANNER = '';
		$A_REC[0]="";
		$A_VAL[0]="";	

	}

		print '
		<table class="table" id="aservers">
			<thead>
			  <tr>
				<th>Type</th>
				<th>Record</th>
				<th>Value</th>                
			  </tr>
			</thead>';
			
		for ($i=0;$i<count($A_VAL);$i++)
		{	
			
			$A_REC[$i] = rtrim ( $A_REC[$i], "." );
			print '
			<tr id="ATR'.$i.'"> 
			<td>A</td>
			<td><input type="text" name="A_REC['.$i.']" value="'.$A_REC[$i].'"></td>
			<td><input type="text" name="A_VAL['.$i.']" value="'.$A_VAL[$i].'">  ';
			
			print '
			<a class="btn btn-danger" onclick="DelField(this.id);" id="A_D'.$i.'" >
				<span class="btn-label">Delete	
			</span>
			</a>';
			#Create an Add Button			
			print '			
			<a class="btn btn-primary" onclick="addField(\'aservers\',\'A\',10);" id="A_A'.$i.'" >
				<span class="btn-label">Add		
			</span>		
			
			</td></tr>
			';			
		}	

	print '
		</table>';
	#CNAME records
	if (!count($CN_VAL)>0)
	{
		$BANNER = $NO_CN_BANNER;
		print $BANNER;
		$BANNER = '';
		$CN_PREF[0]="";
		$CN_VAL[0]="";	
		$CN_NAME[0]="";	
	}

		print '
		<table class="table" id="cnservers">
		<thead>
			  <tr>
				<th>Type</th>
				<th>Preference</th> 
				<th>Value</th> 
				<th>Name</th> 
			  </tr>
			</thead>';
		for ($i=0;$i<count($CN_VAL);$i++)
		{				
			print '
			<tr id="CNTR'.$i.'"> 
			<td>CNAME</td><td>'.
			'<input type="text" name="CN_PREF['.$i.']" value="'.$CN_PREF[$i].'"></td><td>
			<input type="text" name="CN_VAL['.$i.']" value="'.$CN_VAL[$i].'"></td><td>
			<input type="text" name="CN_NAME['.$i.']" value="'.$CN_NAME[$i].'">	';		
			#<button type="submit" name="delete_CN['.$i.']" value="Delete record" class="btn btn-danger"/>Delete</button>';
			print '
			<a class="btn btn-danger" onclick="DelField(this.id);" id="CN_D'.$i.'" >
				<span class="btn-label">Delete	
			</span>
			</a>';
			#Create an Add Button			
			print '			
			<a class="btn btn-primary" onclick="addField(\'cnservers\',\'CN\',10);" id="CN_A'.$i.'" >
				<span class="btn-label">Add		
			</span>		
			
			</td></tr>		
			';
		}

	print '</table>';
	#TXT Records
	if (!count($TXT_VAL)>0)
	{
		$BANNER = $NO_TXT_BANNER;
		print $BANNER;
		$BANNER = '';	
		$TXT_PREF[0]="";
		$TXT_VAL[0]="";	
	}

	print '
		<table class="table" id="txtservers">
		<thead>
			  <tr>
				<th>Type</th>
				<th>Preference</th> 
				<th>Value</th> 
			  </tr>
			</thead>';	
		for ($i=0;$i<count($TXT_VAL);$i++)
		{				
			$TXT_PREF[$i]=trim($TXT_PREF[$i], " ");
			$TXT_VAL[$i]=trim($TXT_VAL[$i], " ");
			print '
			<tr id="TXTTR'.$i.'"> 
			<td>TXT</td><td>'.
			'<input type="text" name="TXT_PREF['.$i.']" value="'.$TXT_PREF[$i].'"></td><td>
			<input type="text" name="TXT_VAL['.$i.']" value=\''.$TXT_VAL[$i].'\'>';
			
			print '
			<a class="btn btn-danger" onclick="DelField(this.id);" id="TXT_D'.$i.'" >
				<span class="btn-label">Delete	
			</span>
			</a>';
			#Create an Add Button			
			print '			
			<a class="btn btn-primary" onclick="addField(\'txtservers\',\'TXT\',10);" id="TXT_A'.$i.'" >
				<span class="btn-label">Add		
			</span>		
			
			
			</td></tr>	
			</table>	
			';
		}	
	
		
	/*
	MAP1
	----
	Map of Buttons and the Functions to which they point. Each button process is processed as POST by the main form.
	---------------
	Update DNS -> ReadAndAssignPostedValues -> PrintPageFormatted
	Add records -> AddNewRecords
	View Zonefile -> PrintZoneFile
	Export Zonefile -> ExportZoneFile
	Save Changes -> SaveZoneFile	
	--------------
	
	*/

		print '<div class="alert alert-info hidden-phone" id="TXTBANNER">
					<a class="close" data-dismiss="alert">×</a>
					<div id="TXTMSG"></div>					
				  </div>

		<button type="submit" name="update_dns" value="Update DNS" class="btn btn-primary"/>Update DNS</button>
		<button type="submit" name="add_new_records" value="Add records" class="btn btn-primary"/>Add records</button>
		<button type="submit" name="show_zonefile" value="Show Zonefile" class="btn btn-primary"/>View Zonefile</button>
		<button type="submit" name="write_zonefile" value="Export Zonefile" class="btn btn-primary"/>Export Zonefile</button>
		<button type="submit" name="save_zonefile" value="Save Changes" class="btn btn-primary"/>Save Changes</button>		
		<a class="btn btn-primary" href="/dns.php"><span class="btn-label">Return</span></a> 		
		</div>			
	  </div>
	</div>
	';
	?>