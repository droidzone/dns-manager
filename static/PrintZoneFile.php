	<?php

	#global $DEBUGON;
	$DEBUGON=true;
	if ($DEBUGON)
		{
			debug_print_backtrace();
			var_dump($_POST);			
		}
	$MAIN_DOMAINNAME=$domain=$_POST['MAIN_DOMAINNAME'];
	$MAIN_NS=$_POST['MAIN_NS'];
	$MAIN_ADMINMAIL=$_POST['MAIN_ADMINMAIL'];
	$DOMAIN_SERIAL=$_POST['DOMAIN_SERIAL'];
	$DOMAIN_REFRESH=$_POST['DOMAIN_REFRESH'];
	$DOMAIN_RETRY=$_POST['DOMAIN_RETRY'];
	$DOMAIN_EXPIRE=$_POST['DOMAIN_EXPIRE'];
	$DOMAIN_NEGTTL=$_POST['DOMAIN_NEGTTL'];
	$DOMAIN_REFRESH=$_POST['DOMAIN_REFRESH'];
	$CONF_FILE=$_POST['CONF_FILE'];
	
	global $MENU_NAME,$MENU_URL;	
	$MENU_NAME=array("Domains","DNS Records","View zonefile");
	BreadCrumb();
	print '
	<h3>Zone file:</h3>
	';
	print '<p>';
	
	#Actual Data
	print $domain. ".". str_repeat('&nbsp;', 5)."IN".spc(5)."SOA".spc(5).$MAIN_NS.spc(5).$MAIN_ADMINMAIL."\t".' (';
	print '<br />';
	print spc(25).$DOMAIN_SERIAL.spc(10).'; Serial';
	print '<br />';
	print spc(25).$DOMAIN_REFRESH.spc(10).'; Refresh';
	print '<br />';
	print spc(25).$DOMAIN_RETRY.spc(10).'; Retry';
	print '<br />';
	print spc(25).$DOMAIN_EXPIRE.spc(10).'; Expire';
	print '<br />';
	print spc(25).$DOMAIN_NEGTTL.spc(3).')'.spc(10).'; Negative Cache TTL';
	print '<br />;<br />';
	$arr=$_POST['NS'];
	#print 'Count is '.count($arr);
	foreach ($_POST['NS'] as &$value) {
		print $domain. ".". spc(5)."IN".spc(5).'NS'.spc(5).$value.'<br />';
	}
	if (isset($_POST['MX_PREF']) && isset($_POST['MX_VAL'] ) ) {

		$i=0;
		$MX_PREF=$_POST['MX_PREF'];
		$MX_VAL=$_POST['MX_VAL'];
		foreach ($_POST['MX_PREF'] as &$value) {	
			if ( $MX_PREF[$i] != "" &&  $MX_VAL[$i] != "") {
				print $domain. ".". spc(5)."IN".spc(5).'MX'.spc(5).$MX_PREF[$i].spc(5).$MX_VAL[$i].'<br />';
			$i++;
			}
			
		}
	}
	
	$A_REC=$_POST['A_REC'];
	$A_VAL=$_POST['A_VAL'];
	$i=0;
	foreach ($_POST['A_VAL'] as $key => $value) {
	//foreach ($_POST['A_VAL'] as &$value) {	
		//print "<h3>Entered A..</h3>";
		//print "<p>Key is ".$key.". Rec is ".$A_REC[$key]." Value is ".$A_VAL[$key]."</p>";
		if ( isset($A_VAL[$key]) )	{
			if ( $A_VAL[$key] !== "") {
				//print "<h4>Entered if block</h4>";
				$domain=rtrim($domain, ".");				
				if ( $A_REC[$key] !== "") {
					print $A_REC[$key].".".$domain. ".". spc(5)."IN".spc(5).'A'.spc(5).$A_VAL[$key].'<br />';
				}	else {
					print $domain. ".". spc(5)."IN".spc(5).'A'.spc(5).$A_VAL[$key].'<br />';
				}				
			}	
		}

	}
	if (isset( $_POST['TXT_PREF']) && isset($_POST['TXT_VAL'] )) {

		$TXT_PREF=$_POST['TXT_PREF'];
		$TXT_VAL=$_POST['TXT_VAL'];
		$i=0;
		foreach ($_POST['TXT_VAL'] as &$value) {	
			if ( $TXT_PREF[$i] != "" &&  $TXT_VAL[$i] != "") {
				print $domain. ".". spc(5).$TXT_PREF[$i]. spc(5)."IN".spc(5).'TXT'.spc(5).$TXT_VAL[$i].'<br />';
				$i++;
			}
		}
	}
	
	if (isset( $_POST['CN_PREF']) && isset($_POST['CN_VAL']) && isset($_POST['CN_NAME'] )) {
		$CN_PREF=$_POST['CN_PREF'];
		$CN_VAL=$_POST['CN_VAL'];
		$CN_NAME=$_POST['CN_NAME'];
		$i=0;
		foreach ($_POST['CN_VAL'] as &$value) {	
			if ( $CN_VAL[$i] != "" &&  $CN_PREF[$i] != "" && $CN_NAME[$i] != "") {
				print $CN_VAL[$i].".".$domain. ".". spc(5).$CN_PREF[$i]. spc(5)."IN".spc(5).'CNAME'.spc(5).$CN_NAME[$i].'<br />';
				$i++;
			}

		}
	}
	print '</p>
	<p>
	<form action="" method="post" name="FormProcessor">
	<input type="hidden" name="MAIN_DOMAINNAME" value="'.$MAIN_DOMAINNAME.'">
	<input type="hidden" name="CONF_FILE" value="'.$CONF_FILE.'">	
	<input type="hidden" name="MAIN_NS" value="'.$MAIN_NS.'">
	<input type="hidden" name="MAIN_ADMINMAIL" value="'.$MAIN_ADMINMAIL.'">
	<input type="hidden" name="DOMAIN_SERIAL" value="'.$DOMAIN_SERIAL.'">
	<input type="hidden" name="DOMAIN_REFRESH" value="'.$DOMAIN_REFRESH.'">
	<input type="hidden" name="DOMAIN_RETRY" value="'.$DOMAIN_RETRY.'">
	<input type="hidden" name="DOMAIN_EXPIRE" value="'.$DOMAIN_EXPIRE.'">
	<input type="hidden" name="DOMAIN_NEGTTL" value="'.$DOMAIN_NEGTTL.'">
	<input type="hidden" name="DOMAIN_REFRESH" value="'.$DOMAIN_REFRESH.'">';
	
	$i=0;
	foreach ($_POST['NS'] as &$value) {
		print '
		<input type="hidden" name="NS['.$i.']" value="'.$value.'">';
		$i++;
	}
	
	$i=0;
	foreach ($_POST['MX_PREF'] as &$value) {	
		print '
		<input type="hidden" name="MX_PREF['.$i.']" value="'.$MX_PREF[$i].'">
		<input type="hidden" name="MX_VAL['.$i.']" value="'.$MX_VAL[$i].'">';
		$i++;
	}
	$i=0;
	foreach ($_POST['A_VAL'] as $key => $value) {
	//foreach ($_POST['A_VAL'] as &$value) {	
		//print "<h3>Entered A..</h3>";
		//print "<p>Key is ".$key.". Rec is ".$A_REC[$key]." Value is ".$A_VAL[$key]."</p>";
		if ( isset($A_VAL[$key]) )	{
			if ( $A_VAL[$key] !== "") {
				//print "<h4>Entered if block</h4>";
				$A_REC[$key] = rtrim ( $A_REC[$key], "." );
				print '
				<input type="hidden" name="A_REC['.$key.']" value="'.$A_REC[$key].'">
				<input type="hidden" name="A_VAL['.$key.']" value="'.$A_VAL[$key].'">';
			}	
		}

	}
	$i=0;
	foreach ($_POST['TXT_VAL'] as &$value) {	
		print '
		<input type="hidden" name="TXT_PREF['.$i.']" value="'.$TXT_PREF[$i].'">
		<input type="hidden" name="TXT_VAL['.$i.']" value=\''.$TXT_VAL[$i].'\'>';
		$i++;
	}
	$i=0;
	foreach ($_POST['CN_VAL'] as &$value) {	
		print '
		<input type="hidden" name="CN_VAL['.$i.']" value="'.$CN_VAL[$i].'">
		<input type="hidden" name="CN_PREF['.$i.']" value="'.$CN_PREF[$i].'">
		<input type="hidden" name="CN_NAME['.$i.']" value="'.$CN_NAME[$i].'">
		';		
		$i++;
	}
	$i=0;	
	print '
	
	<button type="submit" class="btn btn-primary" name="update_dns" value="Return">Return</button>
	</form>	
	</p>
	';
	print '</body></html>';

	?>