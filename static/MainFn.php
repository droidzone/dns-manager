<?php
global $DEBUGMSG;	
global $BANNER, $DBANNER;
global $DEBUGON;
if ($DEBUGON)
	{
		debug_print_backtrace();
		var_dump($_POST);			
	}
global $DATASAVED;
global $SOA_LINE,$MAIN_DOMAINNAME,$MAIN_NS,$MAIN_ADMINMAIL,$SEC_BRACKET,$NS,$MX,$CN,$TXT,$A,$MX_PREF,$MX_VAL,$CN_PREF,$CN_VAL,$CN_NAME;
global $TXT_VAL, $TXT_PREF;
global $A_REC, $A_VAL;
global $CONF_FILE,$DOMAIN_SERIAL,$DOMAIN_REFRESH, $DOMAIN_RETRY, $DOMAIN_EXPIRE, $DOMAIN_NEGTTL;
#print "Value of CONF_FILE (main) is ".$CONF_FILE;
$filename=$CONF_FILE;
if (isset($_POST['create_zonefile']))
{
	CreateZoneFile();
	exit;
}
if (isset($_POST) && !empty($_POST))
{
	if ($DEBUGON)
	{
		debug_print_backtrace();
		var_dump($_POST);			
	}
	#Form was submitted from itself. Read submitted values and print them	
	#Read which domain was edited, from Domain list
	if (isset($_POST['EDIT_ZONE']))
	{
		while (list($key, $val) = each($_POST['EDIT_ZONE'])) {			
				$delkey=$key;
		}	
	}
	#print "Edit zone key is ".$delkey;
	if (isset($_POST['ZONEFILE']))
	{
		$file_list=$_POST['ZONEFILE'];
		$filename=$CONF_FILE=$file_list[$delkey];
	}
	else
	{
		$filename=$CONF_FILE;
	}
	
	#Check if unsaved data exists
	if (isset($_POST['FORMSENDER']))
	{	
		$FORMSENDER=$_POST['FORMSENDER'];
		if ( $FORMSENDER == "AddNewRecords" )
		{
			$DATASAVED=false;
		}
	}
	if (isset($_POST['ZONEFILE']))
	{			
		ReadAndProcessFile();			
		PrintPageFormatted();
	}
	elseif (isset($_POST['show_zonefile']))
	{
		PrintZoneFile();
	}		
	elseif (isset($_POST['update_dns']))
	{
		ReadAndAssignPostedValues();
		PrintPageFormatted();		
	}
	elseif (isset($_POST['write_zonefile']))
	{
		ExportZoneFile();
	}
	elseif (isset($_POST['save_zonefile']))
	{
		SaveZoneFile();
	}		
	elseif (isset($_POST['add_new_records']))
	{
		AddNewRecords();
	}	
	elseif (isset($_POST['cancel_edits']))
	{
		Cancel_Edits();
		PrintPageFormatted();	
	}	
	else
	{
		if (isset($_POST['delete_NS']))
		{				
			while (list($key, $val) = each($_POST['delete_NS'])) {			
				$delkey=$key;
			}
			ReadAndAssignPostedValues();				
			unset ($NS[$delkey]);
			$NS=array_values($NS);				
			$BANNER=$DBANNER;
			PrintPageFormatted();	
		}
		elseif (isset($_POST['delete_MX']))
		{				
			while (list($key, $val) = each($_POST['delete_MX'])) {			
				$delkey=$key;
			}
			ReadAndAssignPostedValues();								
			unset ($MX_PREF[$delkey]);
			unset ($MX_VAL[$delkey]);
			$MX_PREF=array_values($MX_PREF);
			$MX_VAL=array_values($MX_VAL);				
			$BANNER=$DBANNER;
			PrintPageFormatted();					
		}
		elseif (isset($_POST['delete_A']))
		{				
			while (list($key, $val) = each($_POST['delete_A'])) {			
				$delkey=$key;
			}
			ReadAndAssignPostedValues();	
			#print "Delete key ".$delkey;
			unset ($A_REC[$delkey]);
			unset ($A_VAL[$delkey]);
			$A_REC=array_values($A_REC);
			$A_VAL=array_values($A_VAL);				
			$BANNER=$DBANNER;
			PrintPageFormatted();					
		}
		elseif (isset($_POST['delete_CN']))
		{				
			while (list($key, $val) = each($_POST['delete_CN'])) {			
				$delkey=$key;
			}
			ReadAndAssignPostedValues();	
			#print "Delete key ".$delkey;
			unset ($CN_PREF[$delkey]);
			unset ($CN_VAL[$delkey]);
			unset ($CN_NAME[$delkey]);
			$CN_PREF=array_values($CN_PREF);
			$CN_VAL=array_values($CN_VAL);	
			$CN_NAME=array_values($CN_NAME);				
			$BANNER=$DBANNER;
			PrintPageFormatted();					
		}
		elseif (isset($_POST['delete_TXT']))
		{				
			while (list($key, $val) = each($_POST['delete_TXT'])) {			
				$delkey=$key;
			}
			ReadAndAssignPostedValues();	
			#print "Delete key ".$delkey;
			unset ($TXT_PREF[$delkey]);
			unset ($TXT_VAL[$delkey]);				
			$TXT_PREF=array_values($TXT_PREF);
			$TXT_VAL=array_values($TXT_VAL);							
			$BANNER=$DBANNER;
			PrintPageFormatted();					
		}
		
		
		
	}
}
else
{
	$filename=$CONF_FILE;
	#Read values from file when script is called for the first time.
	print "Filename is ".$filename;
	if (is_readable($filename))
	{
		ReadAndProcessFile();			
		PrintPageFormatted();
	}
	else
	{
		hp("File is NOT readable");
		header("Location: /dns.php");
	}
}
?>