<?php

global $DEBUGON;
	if ($DEBUGON)
			{
				debug_print_backtrace();
				var_dump($_POST);			
			}
	global $SOA_LINE,$MAIN_DOMAINNAME,$MAIN_NS,$MAIN_ADMINMAIL,$SEC_BRACKET,$NS,$MX,$CN,$TXT,$A;
	global $A_REC, $A_VAL;
	global $MX_PREF,$MX_VAL,$CN_PREF,$CN_VAL,$CN_NAME;
	global $TXT_VAL, $TXT_PREF;
	global $CONF_FILE,$DOMAIN_SERIAL,$DOMAIN_REFRESH, $DOMAIN_RETRY, $DOMAIN_EXPIRE, $DOMAIN_NEGTTL;	
	$filename=$CONF_FILE;

	$filecont=file_get_contents($filename);
	#Split by selecting bracket as range indicators
	$haystack=$filecont;
	$needle="(";
	$a=stripos($haystack,$needle);		
	#print "First bracket at ".$a;
	$haystack=$filecont;
	$needle=")";
	$b=stripos($haystack,$needle);		
	#print "Second bracket at ".$b;
	$len=$b-$a;
	$seg=substr($haystack,$a+1,$len-1);
	#print $seg;
	#Delete text between [] square brackets, and the brackets themselves
	#http://stackoverflow.com/questions/2174362/remove-text-between-parentheses-php
	$seg=preg_replace("/\[[^\]]+\]/","",$seg);
	#print "<p>$seg</p>";
	#Delete all whitespace
	$seg=preg_replace('/\s+/', '', $seg);
	#Delete all except digits		
	$seg=preg_replace( '/[^0-9;]/', '', $seg);
	#print "Segment:".$seg;
	$arr=explode ( ";", $seg);
	#print_r($arr);
	$DOMAIN_SERIAL=$arr[0];
	$DOMAIN_REFRESH=$arr[1];
	$DOMAIN_RETRY=$arr[2];
	$DOMAIN_EXPIRE=$arr[3];
	$DOMAIN_NEGTTL=$arr[4];
	
	#GET MAIN
	$arr=file($filename);		
	for ($i=0;$i<count($arr);$i++)
	{			
		parseZoneRecord($arr[$i],$i);
	}
	for ($i=0;$i<count($NS);$i++)
	{	
		$NS[$i]=removeSpec($NS[$i],$MAIN_DOMAINNAME.".");
		$NS[$i]=removeSpec($NS[$i],"NS");
		$NS[$i]=removeSpec($NS[$i],"IN");
		#Delete all whitespace
		$NS[$i]=preg_replace('/\s+/', '', $NS[$i]);
	}	
	for ($i=0;$i<count($A);$i++)
	{	
		$A[$i]=removeSpec($A[$i],$MAIN_DOMAINNAME.".");		
		$arr = preg_split('/\s+/', $A[$i]);					
		$A_REC[$i]=$arr[0];
		$A_VAL[$i]=$arr[3];	
		
	}
	for ($i=0;$i<count($MX);$i++)
	{	
		
		$MX[$i]=removeSpec($MX[$i],$MAIN_DOMAINNAME.".");
		$MX[$i]=removeSpec($MX[$i],"MX");
		$MX[$i]=removeSpec($MX[$i],"IN");
		#Split by whitespace, into array
		$arr = preg_split('/\s+/', $MX[$i]);			
		#$arr=array_values($arr);	
		$MX_PREF[$i]=$arr[1];
		$MX_VAL[$i]=$arr[2];	
	}	
	for ($i=0;$i<count($CN);$i++)
	{	
		$CN[$i]=removeSpec_CNAME($CN[$i],$MAIN_DOMAINNAME);
		$CN[$i]=removeSpec($CN[$i],"CNAME");
		$CN[$i]=removeSpec($CN[$i],"IN");
		$arr = preg_split('/\s+/', $CN[$i]);			
		$CN_VAL[$i]=$arr[0];
		$CN_PREF[$i]=$arr[1];	
		$CN_NAME[$i]=$arr[2];		
	}
	for ($i=0;$i<count($TXT);$i++)
	{	
		#print $TXT[$i];
		$TXT[$i]=removeSpec($TXT[$i],$MAIN_DOMAINNAME.".");
		#print "Here:".$TXT[$i];
		$TXT[$i]=removeSpec($TXT[$i],"TXT");
		$TXT[$i]=removeSpec($TXT[$i],"IN");
		#print_r($CN);
		$arr = preg_split('/\s+/', $TXT[$i]);			
		#$arr=array_values($arr);
		#print_r($arr);		
		
		#$TXT_VAL[$i]=$arr[0];
		$TXT_PREF[$i]=$arr[1];	
		$con="";
		for ($j=2;$j<count($arr);$j++)
		{
			$con .= " ".$arr[$j];
		}
		#print "Con is ".$con;
		$TXT_VAL[$i]=$con;
	}
?>