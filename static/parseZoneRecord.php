<?php
	global $DEBUGON;
	if ($DEBUGON)
			{
				debug_print_backtrace();
				var_dump($_POST);			
			}
	global $SOA_LINE,$MAIN_DOMAINNAME,$MAIN_NS,$MAIN_ADMINMAIL,$NS,$MX,$CN,$TXT,$A,$Serial_LINE;
	
	if (strpos($record,"SOA"))
	{
		
		#hp("Main zone header found at line:".$index);
		$SOA_LINE=$index;		
		#Split by whitespace
		$header = preg_split('/\s+/', $record);
		$header=array_values($header);
		#print_r($header);
		$MAIN_DOMAINNAME=rtrim($header[0],'.');		
		$MAIN_NS=$header[3];
		$MAIN_ADMINMAIL=$header[4];		
	}
	if (strpos($record,"Serial"))
	{
		
		#hp("Serial found at line:".$index);
		$Serial_LINE=$index;		
	}
	if (strpos($record,"NS"))
	{
		$NS[]=$record;
	}
	if (strpos($record,"MX"))
	{
		$MX[]=$record;
	}
	if (strpos($record,"CNAME"))
	{
		$CN[]=$record;
	}
	if (strpos($record,"TXT"))
	{
		$TXT[]=$record;
	}
	if ( preg_match("/\bA\b/", $record))
	{
		#print "Debug...".$record;
		$A[]=$record;
	}
?>