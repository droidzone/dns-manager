<?php
$BANNER='';
$DBANNER='		  <div class="alert alert-info hidden-phone">
					<a class="close" data-dismiss="alert">×</a>
					<b>Deleted!</b> The selected record was deleted successfully.					
				  </div>';
$NO_NS_BANNER='<div class="alert alert-info hidden-phone">
					<a class="close" data-dismiss="alert">×</a>
					You have no nameserver records.					
				  </div>';
$NO_MX_BANNER='<div class="alert alert-info hidden-phone id="mxwarning">
					<a class="close" data-dismiss="alert">×</a>
					You have no mailserver records.					
				  </div>';
$NO_A_BANNER='<div class="alert alert-warning hidden-phone" id="NO_A_BANNER">
					<a class="close" data-dismiss="alert">×</a>
					<b>Warning!</b> You have no A records. At least one A record pointing to the server IP is required!					
				  </div>';
$NO_CN_BANNER='<div class="alert alert-info hidden-phone">
					<a class="close" data-dismiss="alert">×</a>
					You have no CNAME records.					
				  </div>';		
$NO_TXT_BANNER='<div class="alert alert-info hidden-phone" id="TXTABANNER">
					<a class="close" data-dismiss="alert">×</a>
					You have no TXT records.					
				  </div>';		
$DATAUNSAVED_ALERT='
	<div class="alert alert-danger hidden-phone">
	<a class="close" data-dismiss="alert">×</a>
	<b>Data not saved!</b>You may have unsaved changes in your zonefile edits. Please commit them if you wish to retain changes made since last save.					
	</div>';				  		
?>				  