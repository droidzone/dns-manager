<?php

global $DATASAVED, $DATAUNSAVED_ALERT;
$DATASAVED=false;
global $DEBUGON;
if ($DEBUGON)
	{
		debug_print_backtrace();
		var_dump($_POST);			
	}

$CONF_FILE=$_POST['CONF_FILE'];
$MAIN_DOMAINNAME=$domain=$_POST['MAIN_DOMAINNAME'];
$MAIN_NS=$_POST['MAIN_NS'];
$MAIN_ADMINMAIL=$_POST['MAIN_ADMINMAIL'];
$DOMAIN_SERIAL=$_POST['DOMAIN_SERIAL'];
$DOMAIN_REFRESH=$_POST['DOMAIN_REFRESH'];
$DOMAIN_RETRY=$_POST['DOMAIN_RETRY'];
$DOMAIN_EXPIRE=$_POST['DOMAIN_EXPIRE'];
$DOMAIN_NEGTTL=$_POST['DOMAIN_NEGTTL'];
$DOMAIN_REFRESH=$_POST['DOMAIN_REFRESH'];

global $MENU_NAME,$MENU_URL;	
$MENU_NAME=array("Domains","Add new records");
BreadCrumb();

#Close the DIV tags of the Main page.
print '
<div class="page-header">
<h3>Add new records</h3>
</div>
<div>
<h4>Domain Name:'.$MAIN_DOMAINNAME.'</h4>
<h5>Zone file: '.$CONF_FILE.'</h5>
';
#print_r($_POST);
print ' 
<form action="" method="post" name="FormProcessor">
<input type="hidden" name="FORMSENDER" value="AddNewRecords">
<input type="hidden" name="MAIN_DOMAINNAME" value="'.$MAIN_DOMAINNAME.'">
<input type="hidden" name="CONF_FILE" value="'.$CONF_FILE.'">
<input type="hidden" name="MAIN_NS" value="'.$MAIN_NS.'">
<input type="hidden" name="MAIN_ADMINMAIL" value="'.$MAIN_ADMINMAIL.'">
<input type="hidden" name="DOMAIN_SERIAL" value="'.$DOMAIN_SERIAL.'">
<input type="hidden" name="DOMAIN_REFRESH" value="'.$DOMAIN_REFRESH.'">
<input type="hidden" name="DOMAIN_RETRY" value="'.$DOMAIN_RETRY.'">
<input type="hidden" name="DOMAIN_EXPIRE" value="'.$DOMAIN_EXPIRE.'">
<input type="hidden" name="DOMAIN_NEGTTL" value="'.$DOMAIN_NEGTTL.'">
<input type="hidden" name="DOMAIN_REFRESH" value="'.$DOMAIN_REFRESH.'">';

$i=0;
if (isset($_POST['NS']))
{
foreach ($_POST['NS'] as &$value) {
	print '
	<input type="hidden" name="NS['.$i.']" value="'.$value.'">';
	$lastNS=$i;
	$i++;
}
print '<input type="hidden" name="lastNS" value="'.$lastNS.'">';
}

$MX_PREF=$_POST['MX_PREF'];
$MX_VAL=$_POST['MX_VAL'];
$i=0;
foreach ($_POST['MX_PREF'] as &$value) {	
print '
<input type="hidden" name="MX_PREF['.$i.']" value="'.$MX_PREF[$i].'">
<input type="hidden" name="MX_VAL['.$i.']" value="'.$MX_VAL[$i].'">';
$lastMX=$i;
$i++;
}
print '<input type="hidden" name="lastMX" value="'.$lastMX.'">';
$A_REC=$_POST['A_REC'];
$A_VAL=$_POST['A_VAL'];
$i=0;
foreach ($_POST['A_VAL'] as &$value) {	
print '
<input type="hidden" name="A_REC['.$i.']" value="'.$A_REC[$i].'">
<input type="hidden" name="A_VAL['.$i.']" value="'.$A_VAL[$i].'">';
$lastA=$i;
$i++;
}
print '<input type="hidden" name="lastA" value="'.$lastA.'">';
$TXT_PREF=$_POST['TXT_PREF'];
$TXT_VAL=$_POST['TXT_VAL'];
$i=0;
foreach ($_POST['TXT_VAL'] as &$value) {	
print '
<input type="hidden" name="TXT_PREF['.$i.']" value="'.$TXT_PREF[$i].'">
<input type="hidden" name="TXT_VAL['.$i.']" value=\''.$TXT_VAL[$i].'\'>';
$lastTXT=$i;
$i++;
}
print '<input type="hidden" name="lastTXT" value="'.$lastTXT.'">';
$i=0;
$CN_VAL=$_POST['CN_VAL'];
$CN_PREF=$_POST['CN_PREF'];
$CN_NAME=$_POST['CN_NAME'];
foreach ($_POST['CN_VAL'] as &$value) {	
print '
<input type="hidden" name="CN_VAL['.$i.']" value="'.$CN_VAL[$i].'">
<input type="hidden" name="CN_PREF['.$i.']" value="'.$CN_PREF[$i].'">
<input type="hidden" name="CN_NAME['.$i.']" value="'.$CN_NAME[$i].'">
';		
$lastCN=$i;
$i++;
}
print '<input type="hidden" name="lastCN" value="'.$lastCN.'">';
$i=0;	
print '
<form action="" method="post" name="FormProcessor">
NS Records
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
		<th>Type</th>
        <th>Value</th>
      </tr>
    </thead>
    <tbody>
	';
	if (!isset($lastNS))
	{
		$lastNS=0;
	}
	for ($i=($lastNS+1);$i<=($lastNS+1+5);$i++)
	{
	print '
      <tr>
        <td>'.$i.'</td>
		<td>NS</td>
        <td>		                    
		  <input type="text" name="NS['.$i.']" value="">
		</td>				            
      </tr>';
	}
	print '
      </tbody>              
  </table>
  
  
  MX Records
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
		<th>Type</th>
		<th>Preference</th>
        <th>Value</th>
      </tr>
    </thead>
    <tbody>
	';
  for ($i=($lastMX+1);$i<=($lastMX+1+3);$i++)
	{
	print '
      <tr>
        <td>'.$i.'</td>
		<td>MX</td>
        <td>		                    
		  <input type="text" name="MX_PREF['.$i.']" value="">
		</td>
		<td>
		  <input type="text" name="MX_VAL['.$i.']" value="">
		</td>				            
      </tr>';
	}
	print '
      </tbody>              
  </table>	  
  
  A Records
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
		<th>Type</th>
        <th>Record Value (Eg: denver, for denver.mysite.com)</th>
        <th>Value (IP address)</th>
      </tr>
    </thead>
    <tbody>
	';
	for ($i=($lastA+1);$i<=($lastA+1+5);$i++)
	{
	print '
      <tr>
        <td>'.$i.'</td>
		<td>A</td>
        <td>		                    
		  <input type="text" name="A_REC['.$i.']" value="">
		</td>
		<td>
		  <input type="text" name="A_VAL['.$i.']" value="">
        </td>                
      </tr>';
	}
	print '
      </tbody>
  </table>
  
  CNAME Records
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
		<th>Type</th>
		<th>Preference (Eg: 3600) </th>
        <th>Value (Eg: * or mail. etc)</th>
        <th>CNAME entry (Other hostname)</th>
      </tr>
    </thead>
    <tbody>
	';
	for ($i=($lastCN+1);$i<=($lastCN+1+5);$i++)
	{
		print '
      <tr>
        <td>'.$i.'</td>
		<td>CNAME</td>
        <td>		                    
		  <input type="text" name="CN_VAL['.$i.']" value="">
		</td>
		<td>
		  <input type="text" name="CN_PREF['.$i.']" value="">
        </td>      
		<td>
		  <input type="text" name="CN_NAME['.$i.']" value="">
        </td>     				
      </tr>';
	}
	print '
      </tbody>
  </table>
  
  TXT Records
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
		<th>Type</th>
		<th>Preference (Eg: 3600) </th>
        <th>Value (Eg:  "v=spf1 a:hermes.joelns.com" )</th>                
      </tr>
    </thead>
    <tbody>
	';
	for ($i=($lastTXT+1);$i<=($lastTXT+1+2);$i++)
	{
		print '
      <tr>
        <td>'.$i.'</td>
		<td>TXT</td>
        <td>		                    
		  <input type="text" name="TXT_PREF['.$i.']" value="">
		</td>
		<td>
		  <input type="text" name="TXT_VAL['.$i.']" value="">
        </td>      				 				
      </tr>';
	}
	print '
      </tbody>
  </table>
  <div class="row"></div>
  </div>     
  		  <div class="alert alert-danger hidden-phone">
			<a class="close" data-dismiss="alert">×</a>
			<b>Note!</b> Updating DNS does not automatically commit changes to zonefiles. You need to "Save Changes" to commit them.					
		  </div>
<button type="submit" class="btn btn-primary" name="update_dns">Update DNS</button>
<button type="reset" class="btn btn-primary" name="Reset">Reset</button>
<button type="submit" class="btn btn-primary" name="cancel_edits">Cancel Edits</button>

</div>
</form>
</div>';

print '</body></html>';
?>