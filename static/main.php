<div class="navbar">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dns.php">DNS Manager v0.1.1</a>
          <div class="navbar-content">
            <ul class="nav">
              <li class="active">
                <a href="#">Domains</a> 
              </li>
              <li>
                <a href="#">Profile</a> 
              </li>
              <li>
                <a href="#">Administration</a> 
              </li>
              <li id="liveclock"></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <ul class="nav  nav-pills nav-stacked">
            <li class="active">
              <a href="#">Domain List</a> 
            </li>
            <li>
              <a href="#">DNS Records</a> 
            </li>
          </ul>
          <div class="well">
            <p>
              <b>Tip:</b> You are viewing Domain list. Click on the "Edit zone" button to edit the DNS entries of that domain.</p>
          </div>
        </div>
        <div class="span9">
          <ul class="breadcrumb">
            <li class="active">
              <a href="/dns.php">Main</a>              
            </li>            
          </ul>
          <form action="zone.php" method="post" name="FormProcessor">	 
            <div style="float: right;">
              <button type="submit" name="create_zonefile" class="btn btn-primary" style="float: left;"/>Add domain</button>
              <button type="submit" name="backup_zones" class="btn btn-primary" style="float: left;"/>Backup</button>
              <button type="submit" name="restore_zones" class="btn btn-primary" style="float: left;"/>Restore</button>
            </div>
            <div 
          
		  <p>Domain List:</p>
          <table class="table">
            <thead>
              <tr>
				<th>No:</th>
                <th>Domain</th>
                <th>Type</th>
				<th>File</th>
              </tr>
            </thead>
