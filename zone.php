<?php 
error_reporting(E_ALL);
$AUTHSCRIPT="/var/password_protect.php";
if (file_exists($AUTHSCRIPT)) 
{
	include($AUTHSCRIPT);	
}

$DEBUGON=false;
$DEBUGMSG="";

include 'static/Banners.php';
include 'static/DeclareGlobals.php';	  

function hp ($lin)
{
	if ($DEBUGON)
		{
			debug_print_backtrace();
			var_dump($_POST);			
		}
	#print $lin;
	print "<p>".$lin."</p>";
}

function hphead ()
{
	global $DEBUGON, $MENU_NAME,$MENU_URL;
	if ($DEBUGON)
		{
			debug_print_backtrace();
			var_dump($_POST);			
		}
	include 'static/header.php';
    include 'static/zone_main.php';
}	
	
function hpfooter ()
{
	global $DEBUGON;
	if ($DEBUGON)
			{
				debug_print_backtrace();
				var_dump($_POST);			
			}
	print "</body></html>";
}

function firstpassHeader()
{
if ($DEBUGON)
		{
			debug_print_backtrace();
			var_dump($_POST);			
		}
	print "";
}

function GenBanner($title,$message)
{
	global $DEBUGON;
	if ($DEBUGON)
	{
		debug_print_backtrace();
		var_dump($_POST);			
	}
	global $BANNER;
	if ($message !== "") {
		//print "<h4>The message was ".$message."</h4>";
		$BANNER='		 <div class="alert alert-info hidden-phone">
					<a class="close" data-dismiss="alert">×</a><b>'.$title.'
					</b>'.$message.'			
				  </div>';
	}	else {
		$BANNER='		 <div class="alert alert-error hidden-phone">
					<a class="close" data-dismiss="alert">×</a><b>'.$title.'
					</b>'."An unknown error occured while communicating with the Layer0 script. Please check server logs!".'			
				  </div>';
	}
}
function GenErrorBanner($title,$message)
{
	global $DEBUGON;
	if ($DEBUGON)
	{
		debug_print_backtrace();
		var_dump($_POST);			
	}
	global $BANNER;
	if ($message !== "") {
		//print "<h4>The message was ".$message."</h4>";
		$BANNER='		 <div class="alert alert-error hidden-phone">
					<a class="close" data-dismiss="alert">×</a><b>'.$title.'
					</b>'.$message.'			
				  </div>';
	}	else {
		$BANNER='		 <div class="alert alert-error hidden-phone">
					<a class="close" data-dismiss="alert">×</a><b>'.$title.'
					</b>'."An unknown error occured while communicating with the Layer0 script. Please check server logs!".'			
				  </div>';
	}
}

function BreadCrumb($menuname=array(), $url=array())
{
	include 'static/BreadCrumb.php';

}



function spc($num)
{

	 return str_repeat('&nbsp;', $num);
}

function tpc($num = 1)
{
	
	$k="";
	for ($i=1; $i<=$num; $i++)
	{
		$k .= " ";
	}
	return "\t";

}

function fprint ($text)
{
global $DEBUGON;
if ($DEBUGON)
		{
			debug_print_backtrace();
			var_dump($_POST);			
		}
	global $filehandle;
	fwrite($filehandle, $text."\n");

}

function SaveZoneFile() 
{
	include 'static/SaveZoneFile.php';	
}

function ExportZoneFile()
{
	include 'static/ExportZoneFile.php';		
}

function PrintZoneFile()
{
	include 'static/PrintZoneFile.php';	
}

function AddNewRecords()
{
	include 'static/AddNewRecords.php';
}

function parseZoneRecord($record, $index)
{
	include 'static/parseZoneRecord.php';
	
}

function removeSpec($haystack,$needle)
{
	return str_replace($needle,"",$haystack);
}
function removeSpec_CNAME($haystack,$needle)
{
	#return str_replace($needle,"",$haystack,1);
	$needle='.'.$needle.'.';
	$needle=str_replace('.','\.',$needle);
	$reptext='/'.$needle.'/';
	return preg_replace($reptext, '', $haystack, 1); 
}

function ReadAndProcessFile()
{
	include 'static/ReadAndProcessFile.php';
}

function ReadAndAssignPostedValues ()
{
	include 'static/ReadAndAssignPostedValues.php';	
}
function Cancel_Edits()
{
	include 'static/Cancel_Edits.php';	
	CheckandDeleteNullValues();
}

function CheckandDeleteNullValues()
{
	include 'static/CheckandDeleteNullValues.php';

}

function AlertIfUnsaved()
{
	global $DATASAVED, $DATAUNSAVED_ALERT;
	if ( !$DATASAVED )
	{
		print $DATAUNSAVED_ALERT;
	}
}

function CreateZoneFile()
{
	include 'static/CreateZoneFile.php';
}

function PrintPageFormatted ()
{
	include 'static/PrintPageFormatted.php';
}

function Main ()
{
	include 'static/MainFn.php';
}	
	
if ($DEBUGON)
{
	debug_print_backtrace();
	var_dump($_POST);			
}	

hphead();
Main();
hpfooter();
?>