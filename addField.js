//Add more fields dynamically.
$(document).ready (function()
{ 
	setInterval(function() {
    var date = new Date();
    var D=date.getHours();
    var A;
    if (D > 12)
    {
    	D=D-12;
    	A=" PM";
    } else {
    	A=" AM"
    }
    var a=pad(D) + ":" + pad(date.getMinutes()) + ":" + pad(date.getSeconds())+A;
    $('#liveclock').html( a);
	}, 1000);

	var RNAME_HELP=" A <domain-name> which specifies the mailbox of the person responsible for this zone. Emails of the format someone@domain.com should be converted to somone.domain.com";
	var REFRESH_HELP="A 32 bit time interval before the zone should be refreshed.";
	var RETRY_HELP="A 32 bit time interval that should elapse before a failed refresh should be retried.";
	var EXPIRE_HELP="A 32 bit time value that specifies the upper limit on the time interval that can elapse before the zone is no longer authoritative. Ref RFC1035";
	var NEG_HELP="The MINIMUM field of the SOA controls the length of time that the negative result may be cached. Ref RFC1034."
    $("#infomsg").hide();
    $("#infomsg_Domain").hide();
    $("#infomsg_Serial").hide();
    $("#TXTBANNER").css("display", "none").hide().html("");
    
    //alert ( $("#TXTMSG").text());
    
    $('#DomainField').blur(function() {
    	$("#infomsg_Domain").hide();
    	var DomField=$("#DomainField");
    	var DomText=DomField.val();    	
    	console.log("Domain is "+DomText);
    	
    	var Dom = DomText.match(/^([a-z0-9]+\.)?[a-z0-9][a-z0-9-]*\.[a-z]{2,6}$/i, "");
    	
    	if (Dom) {
	    	
	    	var fold="/var/lib/bind/db.";
		  	//alert(fold+DomText);
		  	var ConfFile=$("#ConfFile");
		  	ConfFile.val(fold+DomText);
		  	//ConfFile.attr('type', 'text');
		  	//ConfFile.show();
		  	ConfFile.val(fold+DomText).prop('type','text');
		  	//Block element is unhidden
		  	var BlockID = $("#ConfBlock");
		  	BlockID.css("display", "block");

		  	$("#infomsg_Domain").css("display", "none").hide();
		  	
		  	}
	    else	{
			msg="<p>You need to enter a valid domain name to proceed!<br />Examples of valid domain names are: google.com, and google.co.in</p>";
			document.getElementById("info_actual_Domain").innerHTML=msg;	
		 	$("#infomsg_Domain").show().attr("class", "alert hidden-phone alert-error");			  
    	}

	});
	
	$('#SerialField').focus(function() {
		msg="Serial numbers are 32 bit version numbers, usually of format YYYYMMDDxxxx where xxxx are numbers starting at 0001 every day and incremented by 1 at each zone file update. Refer RFC1035";
		$("#info_actual_Serial").html(msg);
		//document.getElementById("info_actual_Serial").innerHTML=msg;	
		 $("#infomsg_Serial").show();
	 });
		 
	$('#SerialField').blur(function() {
		$("#infomsg_Serial").hide();		
	});
		
	$('#DomainField').focus(function() {
		msg="<p>Domains should be of the form domainname.tld</p>Eg: google.com, yahoo.co.in ";
		document.getElementById("info_actual_Domain").innerHTML=msg;	
		 $("#infomsg_Domain").show().attr("class", "alert hidden-phone alert-info");
	});
	
	$('#DOMAIN_REFRESH').focus(function() {
		msg=REFRESH_HELP;
		document.getElementById("info_actual_Serial").innerHTML=msg;	
		 $("#infomsg_Serial").show();
	});		 
	$('#DOMAIN_REFRESH').blur(function() {
		$("#infomsg_Serial").hide();		
	});
		
	$('#DOMAIN_RETRY').focus(function() {
		msg=RETRY_HELP;
		document.getElementById("info_actual_Serial").innerHTML=msg;	
		 $("#infomsg_Serial").show();
	});		 
	$('#DOMAIN_RETRY').blur(function() {
		$("#infomsg_Serial").hide();		
	});
	
	$('#DOMAIN_EXPIRE').focus(function() {
		msg=EXPIRE_HELP;
		document.getElementById("info_actual_Serial").innerHTML=msg;	
		 $("#infomsg_Serial").show();
	});		 
	$('#DOMAIN_EXPIRE').blur(function() {
		$("#infomsg_Serial").hide();		
	});
	$('#DOMAIN_NEGTTL').focus(function() {
		msg=NEG_HELP;
		document.getElementById("info_actual_Serial").innerHTML=msg;	
		 $("#infomsg_Serial").show();
	});		 
	$('#DOMAIN_NEGTTL').blur(function() {
		$("#infomsg_Serial").hide();		
	});
	$('#AdminMail').blur(function() {
		ValidateEmail();		
	});
	
	$('#NS_IN\\[0\\]').blur(function() 	{
		CopyNStoMain();		
	} //Close function tag
	) //Close blur parentheses
	;

	$('form').submit(function () 	{
		//Validate before submitting form
		return CheckFields();
	}
	) //Close submit
	;
	$('form').find('input[name^="A_VAL"]').blur(function () 	{
		//Validate before submitting form
		//alert(this.attr('name'));
		//alert ("Calle");
		var calle="";
	}
	) //Close submit
	;
	//
	
});


function pad (str, max) {
	var a=str.toString();
	if (a.length < 2) {
		a="0"+a;
	} 
	return a;
}

function ShowInfoBar(title,msg)
{
	//alert("Title is "+title+" msg is "+msg);
	title="#" + title;
	//var InfoEl=$(Mtype);
	//var msgarea = 
	$("#info_actual").html(msg).css("display", "block");
	$(title).show();
	//document.getElementById(MType);   
	//msgarea.innerHTML=msg;	
	/*
	Temporarily disabling the alert. This is not useful.
	InfoEl.show();
	setTimeout(HideInfoBar, 2000);
	
	*/
}
function HideInfoBar(Mtype)
{
	var InfoEl=$("#infomsg");
	var msgarea = document.getElementById("info_actual");   
	msgarea.innerHTML="";	
	InfoEl.hide();
	
	
}
function ReportID(clicked) 
{  
 alert(clicked);   
} 

function setCaretPosition(elemId, caretPos) {
    var elem = document.getElementById(elemId);

    if(elem != null) {
        if(elem.createTextRange) {
            var range = elem.createTextRange();
            range.move('character', caretPos);
            range.select();
        }
        else {
            if(elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(caretPos, caretPos);
            }
            else
                elem.focus();
        }
    }
}

function CopyDomaintoA ()
{
	var MAINIP=$('#MainIP').val();
	Create_A_Defaults ("aservers", MAINIP);

}

function Create_A_Defaults (area, MAINIP)
{
	if (MAINIP == "") {
		alert("You need to set a default Server IP before setting default A records!");
		return false;
	}
	var field_area = document.getElementById(area);
	var count = field_area.getElementsByTagName("input").length; 
	count=count/2;   	
	//If count is 1, and fields are empty, overwrite them
	if ( count == 1 && $("#A_VAL0").val() == "" ) {		
		$("#A_VAL0").val(MAINIP);
	}	else {
		$("#"+area).append(
   			'<tr id="ATR' + count + '"> '+
   				'<td>A</td>'+
				'<td><input type="text" name="A_REC[' + count + ']" id="A_REC' + count + '" ></td>' + 
				'<td><input type="text" name="A_VAL[' + count + ']" value="' + MAINIP + '" id="A_VAL' + count + '">' + 
				'<a id="A_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="A_A' + count + '" onclick="addField(\'aservers\',\'A\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');
   		
   		//var ELEMENT_ID="A_REC"+"["+count+"]";
   		count++;
	}
	InfoText="A record added!";
	$("#NO_A_BANNER").css("display", "none").hide();	

   		$("#"+area).append(
   			'<tr id="ATR' + count + '"> '+
   				'<td>A</td>'+
				'<td><input type="text" name="A_REC[' + count + ']" id="A_REC' + count + '" value="*"></td>' + 
				'<td><input type="text" name="A_VAL[' + count + ']" value="' + MAINIP + '" id="A_VAL' + count + '">' + 
				'<a id="A_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="A_A' + count + '" onclick="addField(\'aservers\',\'A\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');


}
function CreateRow(area)
{
	/*
	 * Createrow works like this:
	 * - First, you need to set a Table ID for each Record collection-NS, MX, A 
	 * - Next, you assign a unique ID for each row of elements in each collection.
	 * - When Add button is clicked, the area that the element falls in, as determined by the Table ID is passed to the function.
	 * It determines the last count of the element in the area, and adds to the end of the stack in the area.
	 * - When Del button is clicked,
	 * - The function determines the row that was passed, and deletes all elements in that row.
	 * 
	 */
   
  //alert(area);
   var field_area = document.getElementById(area);   
	//Get the count of number of input boxes in the vicinity of the main tag
    var count = field_area.getElementsByTagName("input").length; //Get all the input fields in the given area.
    //Find the count of the last element of the list. It will be in the format <field><number>. If the 
    //		field given in the argument is friend_ the last id will be friend_4.

   // alert("Count is "+count);   
    
  // Note that area matches the Table ID of the collection.
  var InfoText="";
    if (area=="nameservers")
    {
    	/*
    	 * 
    	 * We need to generate the html tag:
    	 * 
    	 * <tr id="NSTR4">
    	 * 	<td>NS</td>
    	 * 	<td>
    	 * 		<input type="text" name="NS[4]">
    	 * 		<a id="NS_D4" class="btn btn-danger">
    	 * 			<span class="btn-label">Delete</span>
    	 * 		</a>
    	 * 		<a class="btn btn-primary" id="NS_A4">
    	 * 			<span class="btn-label">Add</span>
    	 * 		</a>
    	 * 	</td>
    	 * 	</tr>
    	 */    	 
    	
    	InfoText="NS added!";
    	$("#"+area).append(
   			'<tr id="NSTR' + count + '"> '+
   				'<td>NS</td>'+
				'<td><input type="text" name="NS[' + count + ']" id="NS[' + count + ']">' + 				
				'<a id="NS_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="NS_A' + count + '" onclick="addField(\'nameservers\',\'NS\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');
	    var ELEMENT_ID="NS"+"["+count+"]";
    } else if (area=="mxservers")
    {
	/*
    	 * 
    	 * We need to generate the html tag:
    	<tr id="MXTR0"> 
			<td>MX</td>
			<td><input type="text" value="10" style="width: 30px;" name="MX_PREF[0]"></td><td><input type="text" value="aspmx.l.google.com." name="MX_VAL[0]">
			<a id="MX_D0" onclick="DelField(this.id);" class="btn btn-danger">
				<span class="btn-label">Delete	
			</span>
			</a>			
						
			<a id="MX_A0" onclick="addField('mxservers','MX',10);" class="btn btn-primary">
				<span class="btn-label">Add		
			</span>
			
			</a></td></tr>
    */
   		// For MX, we have two <input> tags in each row, so divide count by 2

	count=count/2;
	InfoText="Mailserver record added!";	
	$("#mxwarning").css("display","none");
	// Two fields: MX_PREF and MX_VAL	
	$("#"+area).append(
			'<tr id="MXTR' + count + '"> '+
			'<td>MX</td>' +
		'<td><input type="text" style="width: 30px;" name="MX_PREF[' + count + ']" id="MX_PREF[' + count + ']"></td>' +
		'<td>'+
			'<input type="text" name="MX_VAL[' + count + ']">' +
			'<a id="MX_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
			'<span class="btn-label">Delete	' +
			'</span>' +
			'</a>		' +						
			'<a id="MX_A'  + count + '" onclick="addField(\'mxservers\',\'MX\',10);" class="btn btn-primary">' +
				'<span class="btn-label">Add		' +
			'</span>	' +			
			'</a>' +
		'</td>' +
	'</tr> ');
	var ELEMENT_ID="MX_PREF"+"["+count+"]";

    } else if (area=="aservers")
    {
	/*
    	 * 
    	 * We need to generate the html tag:
    	<tr id="ATR0"> 
			<td>A</td>
			<td><input type="text" value="mail." name="A_REC[0]"></td>
			<td><input type="text" name="A_VAL[0]">  
			<a id="A_D0" onclick="DelField(this.id);" class="btn btn-danger">
				<span class="btn-label">Delete	
			</span>
			</a>			
			<a id="A_A0" onclick="addField('aservers','A',10);" class="btn btn-primary">
				<span class="btn-label">Add					</span>					
			</a></td></tr>
    */
   		// For A, we have two <input> tags in each row, so divide count by 2
   		count=count/2;   		
   		$("#"+area).append(
   			'<tr id="ATR' + count + '"> '+
   				'<td>A</td>'+
				'<td><input type="text" name="A_REC[' + count + ']" id="A_REC' + count + '"></td>' + 
				'<td><input type="text" name="A_VAL[' + count + ']" id="A_VAL' + count + '">' +
				'<a id="A_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="A_A' + count + '" onclick="addField(\'aservers\',\'A\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');

   		InfoText="A record added!";   		
   		if (count == 0) {
   			$("#NO_A_BANNER").css("display", "block").show();
   		}
   		var ELEMENT_ID="A_REC"+"["+count+"]";
    } else if (area=="cnservers")
    {
	/*
    	 * 
    	 * We need to generate the html tag:
    	<tr id="CNTR0"> 
			<td>CNAME</td><td><input type="text" name="CN_PREF[0]"></td><td>
			<input type="text" name="CN_VAL[0]"></td><td>
			<input type="text" name="CN_NAME[0]">	
			<a id="CN_D0" onclick="DelField(this.id);" class="btn btn-danger">
				<span class="btn-label">Delete	
			</span>
			</a>			
			<a id="CN_A0" onclick="addField('cnservers','CN',10);" class="btn btn-primary">
				<span class="btn-label">Add		
			</span>		
			
			</a></td></tr>
    */
   		// For CN, we have three <input> tags in each row, so divide count by 2
   		count=count/3;		
   		$("#"+area).append(
   			'<tr id="CNTR' + count + '"> '+
   				'<td>CNAME</td>'+
				'<td><input type="text" name="CN_PREF[' + count + ']" id="CN_PREF[' + count + ']"></td>' + 
				'<td><input type="text" name="CN_VAL[' + count + ']"></td>' +
				'<td><input type="text" name="CN_NAME[' + count + ']">' +
				'<a id="CN_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="CN_A' + count + '" onclick="addField(\'cnservers\',\'CN\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');
   		InfoText="CNAME record added!";
   		var ELEMENT_ID="CN_PREF"+"["+count+"]";

    } else if (area=="txtservers")
    {
	/*
    	 * 
    	 * We need to generate the html tag:
    	<tr id="TXTTR0"> 
			<td>TXT</td><td><input type="text" value="3501" name="TXT_PREF[0]"></td><td>
			<input type="text" value="&quot;v=spf1 a:hermes.joel.co.in mx:hermes.joel.co.in mx:apollo.kgimoa.comfo ip4:198.23.228.223 ~all&quot;" name="TXT_VAL[0]">
			<a id="TXT_D0" onclick="DelField(this.id);" class="btn btn-danger">
				<span class="btn-label">Delete	
			</span>
			</a>			
			<a id="TXT_A0" onclick="addField('txtservers','TXT',10);" class="btn btn-primary">
				<span class="btn-label">Add		
			</span>		
			
			
			</a></td></tr>
    */
   		// For TXT, we have 2 <input> tags in each row, so divide count by 2
   		count=count/2;
   		$("#"+area).append(
   			'<tr id="TXTTR' + count + '"> '+
   				'<td>TXT</td>'+
				'<td><input type="text" name="TXT_PREF[' + count + ']" id="TXT_PREF[' + count + ']"></td>' + 
				'<td><input type="text" name="TXT_VAL[' + count + ']" id="TXT_VAL[' + count + ']">' +				
				'<a id="TXT_D' + count + '" onclick="DelField(this.id);" class="btn btn-danger">' +
				'<span class="btn-label">Delete' +
				'</span>' +
				'</a>' +
				'<a id="TXT_A' + count + '" onclick="addField(\'txtservers\',\'TXT\',10);" class="btn btn-primary">' +
				'	<span class="btn-label">Add</span>' +
				'</a></td></tr>');
   		InfoText="TXT record added!";

    	var ELEMENT_ID="TXT_PREF"+"["+count+"]";
	    
    } else if (area=="infomsg")
    {
    	var InfoEl="";
    	//$(#infomsg);
    	//InfoEl.text(Date());
    	
	 		
    }
    ShowInfoBar("infomsg",InfoText);
    
    //Move cursor to the start of the newly created field.	    
    setCaretPosition(ELEMENT_ID, 0) ;
}
function DelField (clickedID)
{   
    var IDb=clickedID;   
    //alert(IDb);     
    var n;
    if (	IDb.indexOf("NS_D") != -1 )
    {
    	n="NSTR"+IDb.replace("NS_D","");
    	var area="nameservers";
    } else if (	IDb.indexOf("MX_D") != -1 )
    {
    	n="MXTR"+IDb.replace("MX_D","");
    	var area="mxservers";
    } else if (	IDb.indexOf("A_D") != -1 )
    {
    	n="ATR"+IDb.replace("A_D","");
    	var area="aservers";
    } else if (	IDb.indexOf("CN_D") != -1 )
    {
    	n="CNTR"+IDb.replace("CN_D","");
    	var area="cnservers";
    } else if (	IDb.indexOf("TXT_D") != -1 )
    {
    	n="TXTTR"+IDb.replace("TXT_D","");
    	var area="txtservers";
    }             	
    
    var element = document.getElementById(n);
    element.parentNode.removeChild(element);
    var field_area = document.getElementById(area);
    var all_inputs = field_area.getElementsByTagName("input");
    var count =all_inputs.length;
    if (count==0)
        {
            CreateRow(area);
        }    
}

function addField(area,field,limit) 
{
    CreateRow(area);
}

function addsField(area,field,limit) 
{
    //alert("Hola");
    if(!document.getElementById) return; //Prevent older browsers from getting any further.
    var field_area = $("#area");
    var all_inputs = $("#area input");
    //field_area.getElementsByTagName("input");
    //document.getElementById(area);
    //var all_inputs = field_area.getElementsByTagName("input"); //Get all the input fields in the given area.
    //Find the count of the last element of the list. It will be in the format <field><number>. If the 
    //		field given in the argument is friend_ the last id will be friend_4.
    var last_item = all_inputs.length;
    //var last = all_inputs[last_item].id;
    var count = last_item;
    //Number(last.split("_")[1]) + 1;

    //If the maximum number of elements have been reached, exit the function.
    //		If the given limit is lower than 0, infinite number of fields can be created.
    if(count > limit && limit > 0) return;

    if(document.createElement) 
    { 
        //W3C Dom method.
        var tr = document.createElement("tr");
        tr.id = "NSTR"+count;
        var input = document.createElement("input");
        // If NS is passed, should become NS[2] etc
        input.id = field+"["+count+"]";
        input.name = field+"["+count+"]";
        input.type = "text"; //Type of field - can be any valid input type like text,file,checkbox etc.

        var td=document.createElement("td");
        var newContent = document.createTextNode("NS");
        td.appendChild(newContent);

        tr.appendChild(td);		
        td=document.createElement("td");		
        td.appendChild(input);
        tr.appendChild(td);					

        //Create Button Del
        var btnDel=document.createElement("a");
        btnDel.id="NS_D"+count;
        btnDel.className="btn btn-danger";
        btnDel.addEventListener('click', function() 
        {            
            //alert(this.id);
            var IDb=this.id;           
            var n="NSTR"+IDb.replace("NS_D","");
            //alert(n);
            var element = document.getElementById(n);
            element.parentNode.removeChild(element);
        }, false);
        var btnText=document.createElement("span");
        btnText.className="btn-label";
        btnText.innerHTML="Delete";
        btnDel.appendChild(btnText);	        
        td.appendChild(btnDel);		


        //Create Button Add
        var btnAdd=document.createElement("a");
        btnAdd.className="btn btn-primary";
        btnAdd.onclick = "addField('nameservers','NS',10);" ;	
        btnAdd.addEventListener('click', function() 
        {
            addField(area,field,limit);
        }, false);
        
        var btnText=document.createElement("span");
        btnText.className="btn-label";
        btnText.innerHTML="Add";		
        btnAdd.appendChild(btnText);		
        td.appendChild(btnAdd);

        tr.appendChild(td);			
        field_area.appendChild(tr);

    } else { //Older Method
            field_area.innerHTML += "<li><input name=\'"+(field+count)+"\' id=\'"+(field+count)+"\' type=\'text\' /></li>";
    }
}

function GenSerial()
{
	Date.prototype.yyyymmdd = function() {
	   var yyyy = this.getFullYear().toString();
	   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
	   var dd  = this.getDate().toString();
	   return yyyy + (mm[1]?mm:"0"+mm[0]) + (dd[1]?dd:"0"+dd[0]); // padding
	  };
	
	d = new Date();
	var Sln=$("#SerialField");
  	Sln.val(d.yyyymmdd()+"0001");
}  	

function IncrementSerial()
{
	a=parseInt($("#SerialField").val());
	
	$("#SerialField").val(a+1);
	//#$("#SerialField").val($("#SerialField").val+1);		
}

function ClearSOA () {
	$("#DOMAIN_REFRESH").val("");
  	$("#DOMAIN_RETRY").val("");
  	$("#DOMAIN_EXPIRE").val("");
  	$("#DOMAIN_NEGTTL").val("");
}

function DefaultSOA () {	
  	$("#DOMAIN_REFRESH").val("86400");
  	$("#DOMAIN_RETRY").val("7200");
  	$("#DOMAIN_EXPIRE").val("3600000");
  	$("#DOMAIN_NEGTTL").val("172800");
  	$("#infomsg_Serial").show();
  	$("#info_actual_Serial").html("Recommended defaults have been set.");
  	setTimeout(function(){HideInfoDefaultSOA()},3000);
  	if ( $("#SerialField").val()=="")
  	{
  		GenSerial();
  	}
}
function HideInfoDefaultSOA()
{
	$("#infomsg_Serial").hide();	
}

function ValidateEmail ()
{
	var a=$("#AdminMail").val(); 
	if ( a.indexOf("@") !== -1)
	{		
		$("#AdminMail").val( a.split('@')[0] + "." + a.split('@')[1] +"." );
	}
}

function CopyNStoMain()
{
	var a=$('#NS_IN\\[0\\]').val();	
	var b=$('#DomainField').val();
	if ( b !== "" && a!== "")
	{
		$('#MainNS').val(a+"."+b+".");
		//CopyDomaintoA();
	}
}


	/*

	if ( $('#DomainField').val() !== "" ) {
		//Find count of A records, confirm is 1. If nothing has been added, we can add default fields
		var leng=$('#aservers input[name^="A_VAL"]').length;
		$('#aservers input[name^="A_VAL"]').each(function(index, elem) { 			
			var $aelem = $(elem).attr('name');			
			var val=$(elem).val();
			//alert(val);
			
			var rec=$('#A_REC'+ index).val();
			alert("Inside the selector!"+$aelem + " Index:" +index + " A val is " + val + " A rec is "+rec);
			if (index==0 && val=="" && rec == "")	{
				alert("Empty");
			}
		});

*/

		/*


		if (leng==1)
		{

			alert("You have just one A record");
			var FirstARec=$('#aservers input[name^="A_REC"]').val()[0];
			var FirstAVal=$('#aservers input[name^="A_VAL"]').val()[0];
			alert(FirstARec);
			if (FirstARec == "" && FirstAVal=="") {
				alert("It is empty");
			}
		}
		//alert(leng);

		*/

/*
		$('#aservers input[name^="A_REC"]').each(function(index, elem) { 
			
			var $aelem = $(elem).attr('name');
			alert("Inside the selector!"+$aelem);
		});

		//var field_area = $("#aservers");   
		//Get the count of number of input boxes in the vicinity of the main tag
	    //var count = field_area.getElementsByTagName("input").length; //Get all the input fields in the given area.
	}
}

*/

function NameAlone(st)
{	
	var b=st.indexOf("["); 
	return st.substring(0,b);
}

function IndexAlone(st)
{
	var b=st.indexOf("[")+1; 
	var c=st.indexOf("]");
	return st.substring(b,c);
}

function isnum(st){
	return /^\d+$/.test(st);
}

function CheckFields()
{
	
	//$("input[name=NS\\[0\\]]").val()
	var VALIDATED=true;

	if ( $("#DomainField").val() =="") 	{
		VALIDATED=false;
		//Set background color to Red
     	
    }	else {
		$("#DomainField").css("background-color", "#FFFFFF");	
    }
	
	/* 
	Either use the selector:
	$("form input").each(function ()
	Or
	$(':text')

	*/

	//Hold variables to count number of entries
	var $a_entries=0;
	var $ns_entries=0;
	$(':text').each(function () 
	{
		
	     //Validating CNameservers        
	     var a=$(this).attr("name");	     
	     var b=NameAlone(a);
	     var value=$(this).val();
	     //Check if Preferences is a Digit
	     
	     //alert(b);
	     if (a=="MAIN_DOMAINNAME" || a=="DOMAIN_SERIAL" || a=="DOMAIN_REFRESH" || a=="DOMAIN_RETRY" || a=="DOMAIN_EXPIRE" || a=="DOMAIN_NEGTTL" || a=="MAIN_NS" || a=="MAIN_ADMINMAIL") {
	     	if (value=="") {
	     		VALIDATED=false;	
     			$(this).css("background-color", "#F7B7B7");
     		} else {
     			$(this).css("background-color", "#FFFFFF");
     		}	     		
	     }	else if (b=="NS")	{
	     	if (value=="") {
	     		// VALIDATED=false;	 //NOt compulsory
     			$(this).css("background-color", "#F7B7B7");
     		} else {
				$ns_entries++;
     			$(this).css("background-color", "#FFFFFF");
     		}
	     } else if (b=="MX_PREF")	{
	     	if (value=="") {
	     		//VALIDATED=false;	
     			$(this).css("background-color", "#D8A0C8");
     		} else {
     			$(this).css("background-color", "#FFFFFF");
     		}
	     } else if (b=="MX_VAL")	{
	     	if (value=="") {	     		
	     		//VALIDATED=false;	
     			$(this).css("background-color", "#D8A0C8");
     		} else {
     			$(this).css("background-color", "#FFFFFF");
     		}
	     } else if (b=="A_VAL")	{
	     	if (value=="") {
	     		//VALIDATED=false;	
     			$(this).css("background-color", "#F7B7B7");
     		} else {
     			$a_entries++;
     			$(this).css("background-color", "#FFFFFF");
     		}
	     }	else if (b=="CN_PREF")	     {
	     	if ( (!isnum(value)) || ( isnum(value) && (value < 1) ) )
	     	{
	     		//VALIDATED=false;	
	     		$(this).css("background-color", "#D8A0C8");
	     	} else {
	     		$(this).css("background-color", "#FFFFFF");
	     	}
	     } else if (b=="CN_NAME")	{
	     	/*
	     		var $this = $(this);
	     		$(this).siblings().each ( function {
	     			alert(this);

	     		});
	     		*/
	     		if (value=="")	{
	     			//VALIDATED=false;
	     			$(this).css("background-color", "#D8A0C8");
	     		}	else {
	     		$(this).css("background-color", "#FFFFFF");
	     	}
	     } else if (b=="CN_VAL")	{
	     		if (value=="")	{
	     			//VALIDATED=false;
	     			$(this).css("background-color", "#D8A0C8");
	     		}	else {
	     		$(this).css("background-color", "#FFFFFF");
	     	}
	     } else if (b=="TXT_PREF")	{	     	
	     	if ( (!isnum) || ( isnum && (value < 1) ) )
	     	{
	     		//VALIDATED=false;
	     		$("#TXTBANNER").css("display", "block").show().html("You don't have any TXT records.");	 	     		
	     		$(this).css("background-color", "#D8A0C8");    		
	     	} else {
	     		$(this).css("background-color", "#FFFFFF");
	     		$("#TXTBANNER").css("display", "none").hide().html("");	
	     	}
	     }	else if (b=="TXT_VAL")	{
	     	if (value=="")	{
	     			//VALIDATED=false;
	     			$(this).css("background-color", "#D8A0C8");
	     		}	else {
	     		$(this).css("background-color", "#FFFFFF");
	     	}
	     }	

	}); // End text field search

	//alert($a_entries);
	if ($a_entries<1) {
		VALIDATED=false;		
	}
	if ($ns_entries<1) {
		VALIDATED=false;		
	}
	

	if (!VALIDATED) {
		alert("Your DNS records seem to have error(s). Please review the fields marked in Red (Required), and Purple (Missing, but optional)");
		return false;
	} else {
		alert("Your DNS records seem to be Ok. We'll attempt to update the changes now. Note that if successful, you need to click on the Save changes button");
		return true;
	}


} //End function CheckFields


/*
	     $().find('input[name^="MAIN_DOMAINNAME"]')
	     {
	     	alert ("Found dom");
	     }



	     if (b=="MAIN_DOMAINNAME")	{
	     	alert ("Domain name");
	     	if (value=="")	{
	     			VALIDATED=false;
	     			$(this).css("background-color", "#B7F7B2");
	     		}	else {
	     		$(this).css("background-color", "#FFFFFF");
	    	}
	    }
*/
	     

   
	
//$('#ForminCreateZone').submit();      

	/*
function showDialog()
{
    $("#dialog-modal").dialog(
    {
        width: 600,
        height: 400,
        open: function(event, ui)
        {
            var textarea = $('<textarea style="height: 276px;">');
            $(textarea).redactor({
                focus: true,
                autoresize: false,
                initCallback: function()
                {
                    this.set('<p>Lorem...</p>');
                }
            });
        }
     });
}

*/
